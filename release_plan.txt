// Feature request to development branch
@startuml
start
:Feature Request or bug;
:(Optional) Invest;
:(Multiple) Story;
:(Multiple) Task;
:Create feature branch;
repeat 
  repeat
    :Development;
    :Commit to feature branch;
  repeat while (Auto test and static analysis ok)
repeat while (feature complete and code review ok)
:Commit to development branch;
end
@enduml

// Development branch to deployment
@startuml
start
:Commit to development branch;
if(Regression testing) then (Failed)
  :New Bug;
  end
else (Passed)
  if(Feature complete?) then (No)
    :Continue development;
    end
  else (Yes)
    if(Acceptance testing) then (Failed)
      :New Story or Task;
      end
    else (Passed)
      :Deployment;
      end
    endif
  endif
endif
@enduml
